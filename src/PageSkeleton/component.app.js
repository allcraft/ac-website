import React, {Component, PropTypes} from 'react';

class App extends Component {

  static propTypes = {
    header: PropTypes.object,
    main: PropTypes.object,
    footer: PropTypes.object
  };

  render() {
    const {header, main, footer} = this.props;
    return (
      <div id="container">
        {header}
        {main}
        {footer}
      </div>
    );
  }
}

export default App;
