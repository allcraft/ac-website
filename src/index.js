import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Router, Route, IndexRoute} from 'react-router';
import ReactGA from 'react-ga';

import Footer from './Footer/component.footer';
import NavBar from './Nav/container.nav';

import App from './PageSkeleton/component.app';
import HomePage from './HomePage/container.hp';
import ClientPage from './ClientPage/component.client';
import FreelancePage from './FreelancePage/container.freelance';
import MethodPage from './MethodPage/container.method';
import TeamPage from './TeamPage/container.team';
import ContactPage from './ContactPage/container.contact';

import configureStore from './configureStore';

import './index.less';

const injectTapEventPlugin = require('react-tap-event-plugin');

injectTapEventPlugin();

ReactGA.initialize('UA-88086351-1');

const {store, history} = configureStore();

const onUpdateRoute = () => {
  window.scrollTo(0, 0);
  const pathname = window.location.pathname;
  ReactGA.set({page: pathname});
  ReactGA.pageview(pathname);
};

ReactDOM.render(
  <Provider store={store}>
    <Router history={history} onUpdate={onUpdateRoute}>
      <Route path={'/'} component={App}>
        <IndexRoute components={{main: HomePage, header: NavBar, footer: Footer}}/>
        <Route path="client" components={{main: ClientPage, header: NavBar, footer: Footer}}/>
        <Route path="freelance" components={{main: FreelancePage, header: NavBar, footer: Footer}}/>
        <Route path="method" components={{main: MethodPage, header: NavBar, footer: Footer}}/>
        <Route path="team" components={{main: TeamPage, header: NavBar, footer: Footer}}/>
        <Route path="contact" components={{main: ContactPage, header: NavBar, footer: Footer}}/>
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
);
