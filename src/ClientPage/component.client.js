import React, {Component} from 'react';

import './client.less';

class ClientPage extends Component {
  render() {
    return (
      <section className="client">
        <h2>devenir client</h2>
        <h3>Vous recherchez un partenaire fiable pour développer un logiciel de qualité ?</h3>
        <p style={{textAlign: ''}}>Confiez-nous votre projet si vous avez l’un des challenges suivants :</p>
        <div>
          <ul>
            <li><span className="bullet"/>Time To Market (TTM) court</li>
            <li><span className="bullet"/>Complexité technologique</li>
            <li><span className="bullet"/>Initiatives stratégiques</li>
            <li><span className="bullet"/>Domaine fonctionnel complexe</li>
            <li>
              <span className="bullet"/>
              <span className="return">Difficultés à trouver des développeurs compétents en région parisienne</span>
            </li>
          </ul>
        </div>
        <p>
          Nous construisons votre produit avec des équipes <strong>d’indépendants</strong> exclusivement <strong>basées
          en France</strong>, en méthodologie Agile.</p>
        <p>
          Les équipes de Craftsmen sont encadrées par un <strong>Team Leader</strong> géographiquement
          <strong> proche</strong> du Client, qui garantit la communication, l’efficacité et le bon déroulement du
          projet.
          Son rôle est pluriel : Scrum Master, proxy-Product Owner, garant des métriques du projet.
        </p>
        <p>
          Nous travaillons <strong>exclusivement en Agile</strong>, ce qui nous permet d’être réceptifs au changement,
          souples dans le dispositif mis en oeuvre afin de répondre aux besoins et se concentrer sur les bonnes choses à
          faire pour le produit.
        </p>
        <p>
          Tous les projets AllCraft sont régis par le <a
          href="http://www.contrat-agile.org" rel="noopener noreferrer"
          target="_blank">contrat Agile</a>.
        </p>
        <section className="we-do-not">
          <h2>Ce que nous ne faisons pas</h2>
          <p>Nous n'avons pas vocation à faire du portage d'indépendants ni d'assistance technique.</p>
        </section>
      </section>
    );
  }
}

export default ClientPage;
