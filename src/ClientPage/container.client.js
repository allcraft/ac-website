import {connect} from 'react-redux';

import ClientContent from './component.client';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

const ClientPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientContent);

export default ClientPage;
