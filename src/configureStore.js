import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';

import {browserHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';

import reducers from './reducers';

const logger = createLogger({
  collapsed: true,
  predicate: () => process.env.NODE_ENV === 'development' // eslint-disable-line no-unused-vars
});

const createStoreWithMiddleware = applyMiddleware(
  thunkMiddleware,
  logger
)(createStore);

export default function configureStore(initialState) {
  const store = createStoreWithMiddleware(
    reducers,
    initialState,
    window.devToolsExtension ? window.devToolsExtension() : undefined
  );

  const history = syncHistoryWithStore(browserHistory, store);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./', () => {
      const nextRootReducer = reducers.default;
      store.replaceReducer(nextRootReducer);
    });
  }

  return {store, history};
}
