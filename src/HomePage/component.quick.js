import React, {Component} from 'react';
import {Link} from 'react-router';

import './quick.less';

class Quick extends Component {
  render() {
    return (
      <section className="quick">
        <h2>notre mission</h2>
        <h3>
          Délivrer des produits numériques de haute qualité en méthodologie Agile avec les
          meilleurs développeurs en région.
        </h3>
        <p>
          Nous accompagnons nos clients dans la définition du produit, le
          développement et la mise en production.
        </p>
        <Link to="/method">
          <button className="button-link">en savoir plus</button>
        </Link>
      </section>
    );
  }
}

export default Quick;
