import React, {Component} from 'react';
import Quick from './component.quick';
import Skills from './component.skills';

import './hp.less';

class HomePage extends Component {
  render() {
    return (
      <div className="homepage">
        <Quick/>
        <Skills/>
      </div>
    );
  }
}

export default HomePage;
