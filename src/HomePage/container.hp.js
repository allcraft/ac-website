import {connect} from 'react-redux';

import HomePageContent from './component.hp';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

const HomePage = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePageContent);

export default HomePage;
