import React, {Component} from 'react';

import './skills.less';
import BackEnd from './ico-backend.svg';
import FrontEnd from './ico-frontend.svg';
import Mobile from './ico-mobile.svg';

class Skills extends Component {
  render() {
    return (
      <section className="skills">
        <h2>nos compétences</h2>
        <p>
          Nous développons en Agile des produits robustes, résilients et de qualité :
        </p>
        <div className="categories">
          <div className="column">
            <img alt="back-end" src={BackEnd}/>
            <h3>back-end</h3>
            <ul>
              <li>Serveur d’application API REST</li>
              <li>Micro-service</li>
              <li>Système temps réel</li>
              <li>Ordonnanceur</li>
              <li>Système concurrentiel</li>
            </ul>
          </div>
          <div className="column">
            <img alt="mobile" src={Mobile}/>
            <h3>mobile</h3>
            <div>
              <ul>
                <li>Performance</li>
                <li>Fluidité</li>
                <li>Disponibilité</li>
                <li>MVP</li>
              </ul>
            </div>
          </div>
          <div className="column">
            <img alt="front-end" src={FrontEnd}/>
            <h3>front-end</h3>
            <div>
              <ul>
                <li>Mobile First</li>
                <li>Responsive Design</li>
                <li>Single-Page Application</li>
                <li>Application Réactive</li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Skills;
