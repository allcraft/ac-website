import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

import Logo from './allcraft-dark.svg';
import Menu from './menu.svg';
import Close from './close-dark.svg';

import './nav.less';

class NavBar extends Component {

  static propTypes = {
    goClient: PropTypes.func.isRequired,
    goFreelance: PropTypes.func.isRequired,
    goMethod: PropTypes.func.isRequired,
    goHome: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {isOpen: false, isPhone: window.innerWidth <= 480};
  }

  componentDidMount() {
    // noinspection Eslint
    jQuery(() => {
      // noinspection Eslint
      jQuery(this.highlight).typed({
        strings: ['à Bordeaux', 'à Nantes', 'à Marseille', 'à Lyon', 'à Nice', 'où qu\'ils soient.'],
        typeSpeed: 0
      });
    });
  }

  getColor = pathname => {
    return this.props.location.pathname === pathname ? '#d4bd94' : '#ffffff';
  };

  goFreelance = () => {
    this.closeDrawer();
    this.props.goFreelance();
  };

  goMethod = () => {
    this.closeDrawer();
    this.props.goMethod();
  };

  goHome = () => {
    this.closeDrawer();
    this.props.goHome();
  };

  goClient = () => {
    this.closeDrawer();
    this.props.goClient();
  };

  openDrawer = () => this.setState({isOpen: true});

  closeDrawer = () => this.setState({isOpen: false});

  referenceHighlight = highlight => {
    this.highlight = highlight;
  };

  toggleDrawer = () => this.setState({isOpen: !this.state.isOpen});

  render() {
    const {pathname} = this.props.location;
    const {isOpen} = this.state;

    const isHome = pathname === '/';

    return (
      <header className={`${isHome ? '' : 'others'}`}>
        <div className="nav-bar">
          <Link to="/" className="logo">
            <img src={Logo} alt="allcraft"/>
          </Link>
          <div className="links">
            <Link to="/">
              <span className={pathname === '/' ? 'active' : ''}>accueil</span>
            </Link>
            <Link to="/client">
              <span className={pathname === '/client' ? 'active' : ''}>client</span>
            </Link>
            <Link to="/freelance">
              <span className={pathname === '/freelance' ? 'active' : ''}>freelance</span>
            </Link>
            <Link to="/method">
              <span className={pathname === '/method' ? 'active' : ''}>méthode</span>
            </Link>
            <Link to="/team">
              <span className={pathname === '/team' ? 'active' : ''}>équipe</span>
            </Link>
            <a href="https://blog.allcraft.io">
              <span>blog</span>
            </a>
            <button className="drawer-link" onClick={this.toggleDrawer}>
              <img alt="menu" src={Menu}/>
            </button>
          </div>
        </div>
        {isHome && <h1 className="tag-line">
          Des logiciels de qualité créés par des talents,<br/>
          <span className="highlight" ref={::this.referenceHighlight}>où qu&#39;ils soient.</span>
        </h1>}
        <div className={`drawer ${isOpen ? 'visible' : ''}`}>
          <button onClick={this.toggleDrawer}>
            <img alt="close" src={Close}/>
          </button>
          <Link to="/" className="link" onClick={this.toggleDrawer}>
            <span className={pathname === '/' ? 'active' : ''}>accueil</span>
          </Link>
          <Link to="/client" className="link" onClick={this.toggleDrawer}>
            <span className={pathname === '/client' ? 'active' : ''}>client</span>
          </Link>
          <Link to="/freelance" className="link" onClick={this.toggleDrawer}>
            <span className={pathname === '/freelance' ? 'active' : ''}>freelance</span>
          </Link>
          <Link to="/method" className="link" onClick={this.toggleDrawer}>
            <span className={pathname === '/method' ? 'active' : ''}>méthode</span>
          </Link>
          <Link to="/team" className="link" onClick={this.toggleDrawer}>
            <span className={pathname === '/team' ? 'active' : ''}>équipe</span>
          </Link>
          <a href="https://blog.allcraft.io">
            <span>blog</span>
          </a>
        </div>
      </header>
    );
  }
}

export default NavBar;
