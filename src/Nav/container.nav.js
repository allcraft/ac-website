import {connect} from 'react-redux';
import {browserHistory} from 'react-router';

import NavBar from './component.nav';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({
  goClient: () => browserHistory.push('/client'),
  goFreelance: () => browserHistory.push('/freelance'),
  goMethod: () => browserHistory.push('/method'),
  goHome: () => browserHistory.push('/')
});

const NavBarHeader = connect(
  mapStateToProps,
  mapDispatchToProps
)(NavBar);

export default NavBarHeader;
