import {connect} from 'react-redux';

import FreelanceContent from './component.freelance';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

const FreelancePage = connect(
  mapStateToProps,
  mapDispatchToProps
)(FreelanceContent);

export default FreelancePage;
