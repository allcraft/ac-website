import React, {Component} from 'react';

import Globe from './ico-globe.svg';
import Event from './ico-event.svg';
import Home from './ico-home.svg';
import Chat from './ico-chat.svg';
import CraftsmanSchema from './schema-craftsman.png';
import Skillz from './skillz.png';

import './freelance.less';

class FreelancePage extends Component {
  render() {
    return (
      <section className="freelance">
        <h2>réseau AllCraft</h2>
        <h3>Vous êtes indépendant, habitant en région ?</h3>
        <p>
          Vous aimez profondément le développement logiciel, la qualité, la performance et la résilience du code est un
          engagement pour vous...
        </p>
        <div>
          <ul>
            <li>
              <img src={Globe} alt="globe"/>
              <h2>Rejoignez les meilleurs Craftsmen</h2>
              <p>
                Créez ensemble des produits numériques performants, avec des équipes Agiles distribuées
                géographiquement.
              </p>
            </li>
            <li>
              <img src={Event} alt="événement"/>
              <h2>Partagez vos connaissances</h2>
              <p>
                Une fois par mois durant le AllCraft Day.
              </p>
            </li>
            <li>
              <img src={Home} alt="maison"/>
              <h2>Travaillez d’où vous voulez</h2>
              <p>
                Voyagez une fois par Sprint à la rencontre du client durant la journée Marathon (Démo., Rétro., Sprint
                planning)
              </p>
            </li>
            <li>
              <img src={Chat} alt="chat"/>
              <h2>Animez la communauté AllCraft</h2>
              <p>
                Échangez avec la communauté AllCraft de votre région et accompagnez le changement du mode de travail
                traditionnel
              </p>
            </li>
          </ul>
        </div>
        <h2>Le quotidien d’un crafts(wo)man</h2>
        <img src={CraftsmanSchema} className="schema-craftsman" alt="schéma craftsman allcraft"/>
        <h2 id="join">Rejoignez-nous !</h2>
        <a href="https://skillz.allcraft.io" rel="noopener noreferrer" target="_blank">
          <img className="logo-skillz" src={Skillz} height={200} width={200} alt="logo skillz"/>
        </a>
        <p>
          Pour rejoindre le réseau AllCraft, rien de plus simple : saisissez vos compétences, vos appétences techniques
          et vos disponibilités sur notre outil
          <a
            href="https://skillz.allcraft.io" rel="noopener noreferrer"
            target="_blank"> Skillz</a>. Nous vous contacterons dès que votre profil correspondra à une
          mission projetée.
        </p>
        <p>
          Nous apprendrons alors à nous connaître. Vos capacités à travailler à distance seront
          évaluées, ainsi que votre sensibilité à l’agilité. Viendra le moment du test technique.
        </p>
        <p>N’hésitez plus !</p>
        <p>
          Compléter le formulaire Skillz est sans engagement.
        </p>
      </section>
    );
  }
}

export default FreelancePage;
