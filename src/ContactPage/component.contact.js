import React, {Component, PropTypes} from 'react';

import './contact.less';

class ContactPage extends Component {

  static propTypes = {
    submitContactForm: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {firstname: '', lastname: '', email: '', message: '', tel: '', submitted: false};
  }

  onFirstnameChange = event =>
    this.setState({firstname: event.target.value, submitted: false});

  onLastnameChange = event => this.setState({lastname: event.target.value, submitted: false});

  onEmailChange = event => this.setState({email: event.target.value, submitted: false});

  onMessageChange = event => this.setState({message: event.target.value, submitted: false});

  onTelChange = event => this.setState({tel: event.target.value, submitted: false});

  onSubmitForm = event => {
    event.preventDefault();
    const {firstname, lastname, email, tel, message} = this.state;
    const {submitContactForm} = this.props;
    if (firstname.length > 0 && lastname.length > 0 && email.length > 0 && message.length > 0) {
      submitContactForm(firstname, lastname, tel, email, message);
      this.setState({submitted: true, message: ''});
    }
  };

  render() {
    const {submitted, firstname, lastname, email, message, tel} = this.state;
    return (
      <section className="contact">
        <h2>Nous contacter</h2>
        <div className="form-wrapper">
          <form onSubmit={this.onSubmitForm}>
            <input
              type="text"
              name="firstname"
              required="true"
              placeholder="Votre prénom*"
              value={firstname}
              onChange={this.onFirstnameChange}/>
            <input
              type="text"
              name="lastname"
              required="true"
              placeholder="Votre nom*"
              value={lastname}
              onChange={this.onLastnameChange}/>
            <input
              type="tel"
              name="tel"
              placeholder="Votre téléphone"
              value={tel}
              onChange={this.onTelChange}/>
            <input
              type="email"
              name="email"
              required="true"
              placeholder="Votre email*"
              value={email}
              onChange={this.onEmailChange}/>
            <textarea
              name="message"
              required="true"
              placeholder="Votre message*"
              value={message}
              onChange={this.onMessageChange}/>
            {submitted && <p>
              Message envoyé. Merci.
            </p>}
            <button className="button-link" disabled={submitted}>envoyer</button>
          </form>
        </div>
      </section>
    );
  }
}

export default ContactPage;
