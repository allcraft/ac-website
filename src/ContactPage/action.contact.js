require('es6-promise').polyfill();

// noinspection Eslint
import fetch from 'isomorphic-fetch';

export function contactFormSubmitted() {
  return {
    type: 'FORM_SUBMITTED'
  };
}

export function submitContactForm(firstname, lastname, tel, email, message) {
  return dispatch => {
    const data = new FormData();
    data.append('entry.370168635', firstname);
    data.append('entry.1934095906', lastname);
    data.append('entry.1927134545', email);
    data.append('entry.47149475', tel);
    data.append('entry.1832812214', message);
    const config = {
      mode: 'no-cors',
      method: 'POST',
      body: data
    };
    const url = 'https://docs.google.com/forms/d/e/1FAIpQLSeIJJ1sDP-OqF-xIL1TIRIgQtNHmImr2vL60SbENdP8Od1hiw/' +
      'formResponse';
    return fetch(url,
      config)
      .then(() => dispatch(contactFormSubmitted()))
      .catch(() => {
        // ignore error
      });
  };
}
