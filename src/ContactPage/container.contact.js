import {connect} from 'react-redux';

import ContactContent from './component.contact';
import {submitContactForm} from './action.contact';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  submitContactForm: (firstname, lastname, tel, email, message) => {
    dispatch(submitContactForm(firstname, lastname, tel, email, message));
  }
});

const ContactPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactContent);

export default ContactPage;
