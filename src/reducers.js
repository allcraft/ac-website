import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';

// noinspection JSUnresolvedFunction
const reducer = combineReducers({
  routing: routerReducer
  /* your reducers */
});

export default reducer;
