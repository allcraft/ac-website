import {connect} from 'react-redux';

import TeamContent from './component.team';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

const TeamPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamContent);

export default TeamPage;
