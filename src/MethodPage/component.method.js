import React, {Component} from 'react';

import SchemaTeam from './schema-team.png';
import SchemaAllCraft from './schema-ac.png';
import SchemaAllCraftLandscape from './schema-ac-landscape.png';
import ToolsFrameworks from './tools-frameworks.png';
import Tools from './tools.png';

import './method.less';

class MethodPage extends Component {
  render() {
    return (
      <section className="method">
        <h2>méthodologie</h2>
        <h3>Pour réaliser des projets Agiles de qualité nous mettons en place un dispositif éprouvé :</h3>
        <img src={SchemaTeam} className="schema-team" alt="schéma équipe allcraft"/>
        <h2 id="acdm">le delivery manager</h2>
        <p>
          Le AC Delivery Manager (AC-DM) assure le <strong>bon déroulement</strong> des projets AllCraft sur le plan
          <strong> opérationnel</strong>, <strong>contractuel</strong> et <strong>financier</strong>.
        </p>
        <p>
          Il <strong>facilite</strong> et <strong>garantit</strong> la mise en place d’un cadre agile satisfaisant, et
          l’adoption de <strong>bonnes pratiques de développement</strong>, avec l’aide de l'AC Team Leader;
        </p>
        <p>
          Il est responsable de la mise en place et de la tenue du <strong>kick-off</strong> (réunion de lancement du
          projet), puis, périodiquement, du <strong>COPIL</strong> (COmité de PILotage);
        </p>
        <p>
          Il prend des <strong>décisions fortes</strong> avec le AC Team Leader, l’équipe et le client pour <strong>
          gérer les aléas</strong> rencontrés durant la vie du projet.
        </p>
        <p>
          Plus classiquement, il gère les <strong>aspects administratifs et financiers</strong> (contrat, facturation,
          etc.)
        </p>
        <h2 id="actl">le team leader</h2>
        <p>
          La <strong>captation des besoins et l’accompagnement dans la définition du produit</strong> sont encadrés par
          nos Team Leaders géographiquement proche du client.
        </p>
        <p>
          Le AC Team Leader (AC-TL) a également pour rôle de <strong>faciliter les échanges</strong> entre le client et
          les équipes distribuées. Il participe également au <strong>pilotage du projet</strong> avec le AC Delivery
          Manager.
        </p>
        <p>
          Il est rompu à la communication Hangout, téléphone et sait <strong>coordonner</strong> des équipiers distants,
          il a une excellente <strong>intelligence relationnelle</strong>.
        </p>
        <h2 id="system">le système allcraft</h2>
        <p>
          Nous créons un <strong>lien fort</strong> avec nos clients. Nous nous <strong>engageons à livrer tôt, et
          souvent</strong>.
        </p>
        <p>
          Le Sprint initial (0) est le <strong>Sprint clé</strong> dans lequel les équipiers mettent en place
          <strong> l’intégration continue</strong> et le <strong>déploiement continu</strong>. Nous avons pour objectif
          de pouvoir <strong>livrer à tout moment en pressant un bouton</strong>.
        </p>
        <p>
          L’équipe est <strong>présente physiquement</strong>, 1j par Sprint, lors du <strong>Marathon Day</strong> :
          Demo., Rétrospective et Sprint Planning, pour assurer <strong>cohésion</strong> et
          <strong> compréhension</strong>.
        </p>
        <img src={SchemaAllCraft} className="schema-ac" alt="schéma allcraft"/>
        <img src={SchemaAllCraftLandscape} className="schema-ac-landscape" alt="schéma allcraft"/>
        <h2>philosophie</h2>
        <p>
          Les Craftsmen ont une expérience <strong>vérifiée</strong> et <strong>qualifiée</strong> pour le projet.
        </p>
        <p>
          Seuls les Craftsmen ayant passé la <strong>qualification AllCraft</strong> font partie du réseau éponyme. Ils
          sont <strong>experts dans leurs domaines</strong> et travaillent en méthode Agile.
        </p>
        <p>
          La <strong>qualité</strong> du code est primordiale et <strong>fait partie de la philosophie du Crafts(wo)man
          AllCraft</strong>. L’intégration continue, le déploiement continu, le pair programming (à distance), les tests
          et le refactoring n’ont plus de secret pour eux.
        </p>
        <h2>outils</h2>
        <p>
          Nous réalisons des projets de qualité basés sur des <strong>outils</strong> et des <strong>framework
          innovants</strong> mais éprouvés :
        </p>
        <img src={ToolsFrameworks} className="tools-frameworks" alt="outils et frameworks allcraft"/>
        <p>
          Nous sommes convaincus que <strong>travailler à distance est réalisable</strong> tout en gardant un
          <strong> Time To Market (TTM)</strong> et une <strong>adaptabilité certaine</strong>.
        </p>
        <p>
          Nous mettons en œuvre les meilleurs outils pour chaque projet.
        </p>
        <img src={Tools} className="tools" alt="outils allcraft"/>
      </section>
    );
  }
}

export default MethodPage;
