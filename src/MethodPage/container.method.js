import {connect} from 'react-redux';

import MethodContent from './component.method';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

const MethodPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(MethodContent);

export default MethodPage;
