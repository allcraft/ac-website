# AllCraft Web site

* Uses HTML
* [Nginx](https://www.nginx.com/)

## Prerequisite

Make sure you have [Yarn](https://yarnpkg.com), [rimraf](https://www.npmjs.com/package/rimraf) and [Docker](https://www.docker.com/) installed.

```
brew install yarn
yarn add rimraf
```
 
## Install

To run the web site locally just go : 

```
yarn install
yarn run start
```

And then go to [http://localhost:3001/]()

## Check Amazon AWS

The web site is hosted on Amazon AWS. Once you [log in](https://allcraft.signin.aws.amazon.com/console) you can see all the [instances](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Instances:sort=instanceId). The web site is hosted on instance `i-0cf320a4ba5ff61f1` and the public DNS `ec2-35-156-168-147.eu-central-1.compute.amazonaws.com`. Going to this [URL](ec2-35-156-168-147.eu-central-1.compute.amazonaws.com) you should see the web site.  

You can SSH the machine by going : 

``` 
ssh -i ~/Google\ Drive/ALLCRAFT/Keys/ac_website.pem ubuntu@ec2-35-156-168-147.eu-central-1.compute.amazonaws.com
```

## Deploying a new version of the Web Site

To deploy a new version of the website we need to build it in production mode, create the Docker image, push it to Docker Hub, and then, from the AWS instance, pull it.

### Build the app in production mode

The following command uses npm to build with `NODE_ENV=production`

```
yarn run build:production
```

### Testing locally with Docker

The AllCraft website uses HTTPs only, so you would need a certificate to make it work (like the certificates installed on AWS). To test locally, you can comment the following NGinx configuration :

```
    if ($http_x_forwarded_proto != 'https') {
      rewrite ^ https://$host$request_uri? permanent;
    }
```

And then you can run the container : 

```
docker image build -t allcraft/ac-website:1.1 .          # Build
docker container run -p80:80 allcraft/ac-website:1.1     # Execute the image
```

### Build and push the Docker image our AllCraft Docker HUB

Once the app built in production mode, create the Docker image of our website, and then push it to the [Docker Hub](https://hub.docker.com/r/allcraft/ac-website)

```
docker image build -t allcraft/ac-website:1.1 .          # Build
docker image push allcraft/ac-website:1.1                # Push the image
```

### Pull the Docker image on AWS

Log into the AWS instance, update the `docker-compose.yml` with the new version of the image if needed, and do the following : 

```
docker ps                 # Checks it's up and running
docker-compose down       # Stops the image 
docker-compose pull       # If the image version is the same, you need to pull it
docker-compose up -d      # Starts the image in detached mode
```

## Upgrading Docker compose

If needed, you can update the version of Docker Compose on the AWS instance :

```
sudo -i curl -L "https://github.com/docker/compose/releases/download/1.8.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo -i chmod +x /usr/local/bin/docker-compose
```
