FROM nginx:1.11

MAINTAINER Antonio Goncalves <agoncalves@allcraft.io>

LABEL name="AllCraft Web Site"
LABEL description="Contains all the static information (not the blog nor Skillz) of the AllCraft web site"
LABEL version=1.1
LABEL url="https://allcraft.io/"
LABEL vendor="AllCraft"

EXPOSE 80

COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY index.html /usr/share/nginx/html/index.html
COPY app /usr/share/nginx/html/
COPY favicon /usr/share/nginx/html/favicon
COPY mailjet/7e0bcf495146ea59bf1f49bccec1fedd.txt /usr/share/nginx/html/
